# Cuál es el propósito de este repositorio? #

Dentro de de una serie de artículos sobre Frameworks y patrones de gestión de triggers, este es el repositorio con el código correspondiente a la entrada del blog [Un patrón de triggers simple y potente](http://forcegraells.com/2018/06/15/patron-de-triggers/). 

Incluye las clases creadas por Tony Scott, donde he realizado leves modificaciones para simplificar el código y adaptarlo a mi estilo, y una clase de test muy simple para realizar el test.

Espero que te sea de ayuda.

# Contacto #

Para cualquier duda, sugerencia o comentario puedes contactarme en mi [Email Personal](esteve.graells@gmail.com).

Un saludo.
