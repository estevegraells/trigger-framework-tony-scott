/**
 * Created by 00070129 on 15/06/2018.
 */

trigger AccountTrigger on Account (
        before insert, before update,
        before delete, after insert,
        after update, after delete, after undelete) {

    TriggerFactory.createAndExecuteHandler(AccountHandler2.class);

}