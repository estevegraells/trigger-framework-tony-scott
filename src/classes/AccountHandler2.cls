/*
* @author: Esteve Graells
* @date: Junio 2018
* @description: Implementación del Handler del objeto Account
* */

/* Ejemplo de trigger trivial para el objeto Account
 */
public without sharing class AccountHandler2 implements ITrigger {

    // Variable miembro que almacena los identificadores
    // de Account con los q trabaja nuestra lógica de negocio
    private Set<Id> m_inUseIds = new Set<Id>();

    public void bulkBefore() {

        // En el caso del evento de Delete, obtener
        // las accounts que se quieren borrar
        if (Trigger.isDelete) {

            Set<Id> accIds = Trigger.oldMap.keySet();

            List<Account> accountList = [SELECT Id, Name, (SELECT Id FROM Contacts) FROM Account WHERE Id in :accIds];
            for(Account acc : accountList){
                if (acc.Contacts.size() > 0) m_inUseIds.add(acc.id);

            }
        }
    }

    // No permitir el borrado de Accounts con Opps asignadas
    public void beforeDelete(SObject so) {
        Account myAccount = (Account)so;
        if (m_inUseIds.contains(myAccount.Id)) so.addError('Esta Account no se puede eliminar.');
    }

    public void bulkAfter() {
    }
    public void beforeInsert(SObject so) {
    }
    public void beforeUpdate(SObject oldSo, SObject so) {
    }
    public void afterInsert(SObject so) {
    }
    public void afterUpdate(SObject oldSo, SObject so) {
    }
    public void afterDelete(SObject so) {
    }
    public void andFinally() {
    }
}