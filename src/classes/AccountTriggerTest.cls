/*
* @author: Esteve Graells
* @date: Junio 2018
* @description: Clase de Test para Trigger Account
* */

@IsTest
private class AccountTriggerTest {

    private static integer accountsToCreate = 200;
    @isTest
    static void testTrigger() {

        //Test para el evento de INSERT sobre Accounts
        List<Account> accounts = new List<Account>();
        for (integer i=0; i<accountsToCreate; i++){
            Account acc = new Account(Name='Testing'+i);
            accounts.add(acc);
        }

        List<Database.SaveResult> resultsInsertAccounts = Database.insert(accounts, false);

        for (Database.SaveResult r : resultsInsertAccounts){
            if (r.isSuccess())
                System.assert(r.getErrors().size() == 0);
            else System.debug('-ege- Errors ' + r.getErrors());
        }
        System.assert(resultsInsertAccounts.size() == accountsToCreate);

        //Test para el evento de INSERT sobre Contacts
        List<Contact> contacts = new List<Contact>();
        for ( Account a: accounts ){
            Contact c = new contact(LastName='Testing', AccountId = a.Id);
            contacts.add(c);
        }

        List<Database.SaveResult> resultInsertContacts = Database.insert(contacts, false);

        for (Database.SaveResult r : resultInsertContacts){
            if (r.isSuccess())
                System.assert(r.getErrors().size() == 0);
            else System.debug('-ege- Errors ' + r.getErrors());
        }
        System.assert(resultInsertContacts.size() == accountsToCreate);

        //Test para el evento de UPDATE sobre Accounts
        for ( Account a: accounts ){
            a.Name='TestingUpdate';
        }
        List<Database.SaveResult> resultsUpdateAccounts = Database.update(accounts);
        List<AggregateResult> accountsUpdated = [SELECT count(id) totalUpdated FROM Account WHERE Name LIKE '%TestingUpdate'];
        System.assert(accountsUpdated[0].get('totalUpdated') == accounts.size());

        //TEST del evento de DELETE sobre Accounts (Ninguna de las accounts ha sido borrada, pq no superan la validación en el Handler)
        List<Database.DeleteResult> resultsDeleteAccounts = Database.delete(accounts, false);
        for (Database.DeleteResult r : resultsDeleteAccounts){
            if (r.isSuccess())
                System.assert(r.getErrors().size() == 0);
            else System.debug('-ege- Errors ' + r.getErrors());
        }

        List<Account> currentAccounts = [SELECT id FROM Account];
        Integer s = currentAccounts.size();
        System.debug('-ege- Accounts restantes:' + currentAccounts.size());
        System.assert(currentAccounts.size() == accountsToCreate);
    }
}