/*
 * @author: Esteve Graells
 * @date: junio 2018
 * @description: Las funciones de esta clase:
 * Crear la instancia del Handler del objeto que estemos gestionando (y de ahí su nombre)
 * Enrutar el código hacia la función adecuada del Handler dependiendo de la naturaleza del trigger
 *
 */

public with sharing class TriggerFactory{

    // Función principal donde se realiza la
    // creación dinámica del Handler y el enrutamiento

    public static void createAndExecuteHandler(Type t){
        ITrigger handler = getHandler(t);

        if (Handler == null) throw new TriggerException('No Trigger Handler: ' + t.getName());

        // Ejecutar el enrutado hacia la función que
        // implementa el evento generado

        execute(handler);
    }

    /*
     * Ejecución del enrutado
     */
    private static void execute(ITrigger handler){

        if (Trigger.isBefore){

            // Función de caché
            handler.bulkBefore();

            if (Trigger.isDelete){
                for (SObject so : Trigger.old){
                    handler.beforeDelete(so);
                }
            }

            else if (Trigger.isInsert){
                for (SObject so : Trigger.new){
                    handler.beforeInsert(so);
                }
            }

            else if (Trigger.isUpdate){
                for (SObject so : Trigger.old){
                    handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }

        //Repetimos el esquema hacia las funciones after*

        else{

            handler.bulkAfter();

            if (Trigger.isDelete){
                for (SObject so : Trigger.old){
                    handler.afterDelete(so);
                }
            }

            else if (Trigger.isInsert){
                for (SObject so : Trigger.new){
                    handler.afterInsert(so);
                }
            }

            else if (Trigger.isUpdate){
                for (SObject so : Trigger.old){
                    handler.afterUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }

        // Post-proceso y operaciones DML
        handler.andFinally();
    }

    //Función para la creación del handler

    private static ITrigger getHandler(Type t){
        Object o = t.newInstance();

        // Comprobación de seguridad
        if (o instanceOf ITrigger)return (ITrigger)o;
        else return null;
    }

    /*
     * El código de la Excepcion, puede ser el que deseemos
     */
    public class TriggerException extends Exception {}
}