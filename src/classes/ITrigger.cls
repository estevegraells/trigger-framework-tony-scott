/*
 * @author Esteve Graells
 * @date: junio 2018
 * @description: Para cada uno de los eventos del Trigger la interface
 * implementa un método.
 * Además implementa 2 métodos adicionales para caché de
 * los registros, y un método de finalización, donde por
 * ejemplo, concentrar las operaciones DML, y así evitar
 * ineficiencias y superar límites en el Execution Context
 *
 */

public interface ITrigger
{

    // La característica de todos estos métodos, es que son
    // invocados para cada registro involucrado

    void beforeInsert(SObject so);
    void beforeUpdate(SObject oldSo, SObject so);
    void beforeDelete(SObject so);
    void afterInsert(SObject so);
    void afterUpdate(SObject oldSo, SObject so);
    void afterDelete(SObject so);

    // Los 2 métodos Bulk, son invocados para cachear los
    // registros en variables tipo Map, y así eficientar
    // su consulta

    void bulkBefore();
    void bulkAfter();

    // Este método será invocado al finalizar toda la lógica del
    // trigger. Puede servir para realizar operaciones DML,
    // de limpieza, audit/log, etc.

    void andFinally();
}